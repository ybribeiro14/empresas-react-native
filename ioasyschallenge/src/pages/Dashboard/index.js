import React, { useEffect, useState, useCallback } from 'react';
import { ActivityIndicator, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import api from '../../services/api';

import { signOut } from '../../store/modules/auth/actions';
import { listEnterprises } from '../../store/modules/enterprise/actions';
import Enterprises from '../../components/Enterprises';
import {
  Container,
  Title,
  List,
  LogoutButton,
  FilterInput,
  FilterButton,
  SelectFilter,
  ViweButtonsFilter,
  ClearFilterButton,
  ViewTitle,
} from './styles';

const Dashboard = ({ navigation }) => {
  const enterprises = useSelector(state => state.enterprise.enterprises);
  const types = useSelector(state => state.enterprise.types);
  const [filterText, setFilterText] = useState('');
  const [filterType, setFilterType] = useState(null);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const loadEnterprises = useCallback(async () => {
    setLoading(true);

    const response = await api.get('enterprises');

    dispatch(listEnterprises(response.data.enterprises, 'list'));
    setLoading(false);
  }, [dispatch]);

  useEffect(() => {
    loadEnterprises();
  }, []);

  const handleFilterEnterprises = useCallback(async () => {
    if (filterText === '' && !filterType) {
      Alert.alert('Empty filters', 'Fill in some of the filters.');
      return;
    }

    setLoading(true);
    let queryFilter = '';

    if (filterText !== '') {
      queryFilter = `name=${filterText}`;
    }

    if (filterType) {
      // eslint-disable-next-line prettier/prettier
      queryFilter = `${queryFilter === '' ? queryFilter : `${queryFilter}&`}enterprise_types=${filterType}`;
    }

    const response = await api.get(`enterprises?${queryFilter}`);

    dispatch(listEnterprises(response.data.enterprises, 'filter'));
    setLoading(false);
  }, [filterText, filterType, dispatch]);

  const handleClearFilters = useCallback(() => {
    setFilterText('');
    setFilterType(null);
    loadEnterprises();
  }, [loadEnterprises]);

  const handleLogout = useCallback(() => dispatch(signOut()), [dispatch]);

  return (
    <Container>
      <ViewTitle>
        <Title>Enterprises</Title>
        <LogoutButton onPress={handleLogout}>Log Out</LogoutButton>
      </ViewTitle>

      <FilterInput
        placeholder="Filter by name"
        returnKeyType="next"
        value={filterText}
        onChangeText={setFilterText}
      />

      <SelectFilter
        selectedValue={filterType}
        onValueChange={(itemValue, itemIndex) => setFilterType(itemValue)}
      >
        <SelectFilter.Item label="Select a type" value={null} key={0} />
        {types.map(type => (
          <SelectFilter.Item
            label={type.enterprise_type_name}
            value={type.id}
            key={type.id}
          />
        ))}
      </SelectFilter>

      <ViweButtonsFilter>
        <FilterButton onPress={handleFilterEnterprises}>Filter</FilterButton>
        <ClearFilterButton onPress={handleClearFilters}>
          Clear
        </ClearFilterButton>
      </ViweButtonsFilter>

      {loading ? (
        <ActivityIndicator size="large" color="#f64c75" />
      ) : (
          <List
            data={enterprises}
            keyExtractor={item => String(item.id)}
            renderItem={({ item }) => (
              <Enterprises navigation={navigation} data={item} />
            )}
          />
        )}
    </Container>
  );
};

export default Dashboard;
