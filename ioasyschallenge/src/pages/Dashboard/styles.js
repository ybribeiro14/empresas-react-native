import styled from 'styled-components/native';
import { Picker } from '@react-native-picker/picker';
import Button from '../../components/Button';
import Input from '../../components/Input';

export const Container = styled.SafeAreaView`
  flex: 1;
  padding: 20px;
`;

export const ViewTitle = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const ViweButtonsFilter = styled.View`
  align-items: center;
  justify-content: center;
  flex-direction: row;
`;

export const Title = styled.Text`
  font-size: 30px;
  color: #fff;
  font-weight: bold;
  align-self: center;
`;

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
})``;

export const LogoutButton = styled(Button)`
  background: #f64c75;
  padding: 10px;
`;

export const FilterButton = styled(Button)`
  margin: 10px;
  background: #ff4500;
  width: 150px;
`;

export const ClearFilterButton = styled(Button)`
  margin: 10px;
  background: #2f4f4f;
  width: 150px;
`;

export const FilterInput = styled(Input)`
  margin-top: 10px;
  height: 46px;
  padding: 0;
`;

export const SelectFilter = styled(Picker)`
  font-size: 15px;
  margin-top: 10px;
  color: #fff;
  height: 46px;
  background: rgba(0, 0, 0, 0.1);
  padding: 0 100px;
`;
