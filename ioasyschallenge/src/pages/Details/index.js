import React, { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import api from '../../services/api';
import { Container, Name, Avatar, Description, InfosDetails, TextInfos, TextTitles, ScrollView } from './styles';

const Details = ({ route, navigation }) => {
  const [enterprise, setEnterprise] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function loadEnterprisesDetails() {
      setLoading(true);

      const response = await api.get(`enterprises/${route.params.id}`);

      setEnterprise(response.data.enterprise);

      // dispatch(listEnterprises(response.data.enterprises, 'list'));
      setLoading(false);
    }

    loadEnterprisesDetails();
  }, []);

  return (
    <Container>
      {loading ? (
        <ActivityIndicator size="large" color="#f64c75" />
      ) : (
          <>
            <Avatar
              source={{
                uri: `https://empresas.ioasys.com.br/${enterprise.photo}`,
              }}
            />
            <Name>{enterprise.enterprise_name}</Name>
            <ScrollView>
              <Description>{enterprise.description}</Description>

              <InfosDetails>
                <TextTitles>City:</TextTitles>
                <TextInfos>
                  {`${enterprise.city}, ${enterprise.country}`}
                </TextInfos>
              </InfosDetails>
              <InfosDetails>
                <TextTitles>Type:</TextTitles>
                <TextInfos>
                  {enterprise.enterprise_type?.enterprise_type_name}
                </TextInfos>
              </InfosDetails>
              <InfosDetails>
                <TextTitles>Email:</TextTitles>
                <TextInfos>
                  {enterprise.email_enterprise}
                </TextInfos>
              </InfosDetails>
              <InfosDetails>
                <TextTitles>Shares:</TextTitles>
                <TextInfos>
                  {enterprise.shares}
                </TextInfos>
              </InfosDetails>
              <InfosDetails>
                <TextTitles>Share Price:</TextTitles>
                <TextInfos>
                  {`$ ${enterprise.share_price}`}
                </TextInfos>
              </InfosDetails>
            </ScrollView>

          </>
        )}
    </Container>
  );
};

export default Details;
