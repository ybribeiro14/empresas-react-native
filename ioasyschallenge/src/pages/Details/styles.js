import styled from 'styled-components';

export const Container = styled.SafeAreaView`
  flex: 1;
  align-items: center;
  padding-top: 20px;
`;

export const ScrollView = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
  contentContainerStyle: {
    marginTop: 10,
    paddingBottom: 50,
    paddingLeft: 20,
    paddingRight: 20,
  },
})`
  align-self: stretch;
  background: rgba(100, 149, 237, 0.1);
  margin-top: 10px;
`;

export const InfosDetails = styled.View`
  align-items: center;
  flex-direction: row;
  width: 100%;
  margin-top: 5px;
`;

export const Name = styled.Text`
  font-size: 20px;
  color: #fff;
  font-weight: bold;
  align-self: center;
  margin-top: 20px;
`;

export const Description = styled.Text`
  font-size: 16px;
  color: #fff;
  font-weight: normal;
  text-align: center;
  margin-top: 10px;
  margin-bottom: 10px;
  width: 100%;
`;

export const TextInfos = styled.Text`
  font-size: 16px;
  color: #fff;
  font-weight: normal;
  text-align: left;
  margin-left: 10px;
`;

export const TextTitles = styled.Text`
  font-size: 16px;
  color: #fff;
  font-weight: bold;
  text-align: left;
`;

export const Avatar = styled.Image`
  width: 140px;
  height: 140px;
  border-radius: 70px;
`;
