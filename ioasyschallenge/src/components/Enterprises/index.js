import React from 'react';
import { Container, Left, Avatar, Info, Name, TextInfos } from './styles';

const Enterprises = ({ navigation, data }) => (
  <Container onPress={() => navigation.navigate('Details', { id: data.id })}>
    <Left>
      <Avatar
        source={{
          uri: `https://empresas.ioasys.com.br/${data.photo}`,
        }}
      />
      <Info>
        <Name>{`${data.enterprise_name} (${data.enterprise_type.enterprise_type_name})`}</Name>
        <TextInfos>{`${data.city}, ${data.country}`}</TextInfos>
      </Info>
    </Left>
  </Container>
);
export default Enterprises;
