import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  height: 46px;
  background: #ff4500;
  border-radius: 4px;

  justify-content: center;
  align-items: center;
`;

export const Text = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #fff;
`;
