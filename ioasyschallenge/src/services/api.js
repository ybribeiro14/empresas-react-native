import axios from 'axios';

// Usando emulador

const api = axios.create({
  baseURL: 'https://empresas.ioasys.com.br/api/v1',
});

// Usando device

// const api = axios.create({
//   baseURL: 'http://192.168.0.111:3333',
// });

export default api;
