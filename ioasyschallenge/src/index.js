import 'react-native-gesture-handler';
import React from 'react';
import { View, StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';

import { store } from './store';

import Routes from './routes';

const App = () => (
  <NavigationContainer>
    <Provider store={store}>
      <StatusBar barStyle="light-content" backgroundColor="#6495ED" />
      <View style={{ flex: 1, backgroundColor: '#6495ED' }}>
        <Routes />
      </View>
    </Provider>
  </NavigationContainer>
);

export default App;
