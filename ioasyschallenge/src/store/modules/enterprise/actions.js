export function listEnterprises(enterprises, model) {
  return {
    type: '@enterprise/LIST_ENTERPRISES',
    payload: { enterprises, model },
  };
}

export function listTypes(types) {
  return {
    type: '@enterprise/LIST_TYPES',
    payload: { types },
  };
}
