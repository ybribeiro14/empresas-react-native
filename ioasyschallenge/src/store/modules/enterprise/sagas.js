/* eslint-disable prettier/prettier */
/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable no-confusing-arrow */
/* eslint-disable no-nested-ternary */
import { takeLatest, put, all } from 'redux-saga/effects';
import { listTypes } from './actions';

export function* getTypes({ payload }) {
  try {
    const { enterprises, model } = payload;

    if (model !== 'filter') {
      const listOfTypes = [];

      enterprises.map(enterprise => {
        const filterType = listOfTypes.filter(
          type => type.id === enterprise.enterprise_type.id,
        );

        if (!filterType.length) {
          listOfTypes.push(enterprise.enterprise_type);
        }
      });

      listOfTypes.sort((a, b) =>
        a.enterprise_type_name > b.enterprise_type_name ? 1
          : b.enterprise_type_name > a.enterprise_type_name ? -1
            : 0);

      yield put(listTypes(listOfTypes));
    }
  } catch (err) {
    console.log(err);
  }
}
export default all([takeLatest('@enterprise/LIST_ENTERPRISES', getTypes)]);
