/* eslint-disable no-param-reassign */
import produce from 'immer';

const INITIAL_STATE = {
  enterprises: [],
  types: [],
};

export default function enterprise(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case '@enterprise/LIST_ENTERPRISES': {
        draft.enterprises = action.payload.enterprises;
        break;
      }
      case '@enterprise/LIST_TYPES': {
        draft.types = action.payload.types;
        break;
      }
      case '@auth/SIGN_OUT': {
        draft.enterprise = [];
        draft.types = [];
        break;
      }
      default:
    }
  });
}
