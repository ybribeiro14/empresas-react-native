import { Alert } from 'react-native';
import { takeLatest, call, put, all } from 'redux-saga/effects';

import api from '../../../services/api';

import { signInSuccess, signFailure } from './actions';

export function* signIn({ payload }) {
  try {
    const { email, password } = payload;

    const response = yield call(api.post, 'users/auth/sign_in', {
      email,
      password,
    });
    const customHeaders = response.headers;

    // if (!customHeaders['access-token']) {
    //   Alert.alert('Erro no login', 'Usuário não pode ser prestador de serviço');
    //   return;
    // }

    api.defaults.headers['access-token'] = customHeaders['access-token'];
    api.defaults.headers.client = customHeaders.client;
    api.defaults.headers.uid = customHeaders.uid;

    yield put(signInSuccess(response.data));
  } catch (err) {
    Alert.alert(
      'Falha na autenticação',
      'Houve um erro no login, verifique seus dados.',
    );
    yield put(signFailure());
  }
}

export function* signUp({ payload }) {
  try {
    const { name, email, password } = payload;

    yield call(api.post, 'users', {
      name,
      email,
      password,
    });

    // history.push('/')
  } catch (err) {
    Alert.alert(
      'Falha no cadastro',
      'Houve um erro no cadastro, verifique seus dados.',
    );
    yield put(signFailure());
  }
}

export default all([takeLatest('@auth/SIGN_IN_REQUEST', signIn)]);
