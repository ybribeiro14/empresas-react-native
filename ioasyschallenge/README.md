# Desafio React Native - ioasys

## App desenvolvido seguindo orientações do desafio Ioasys - React Native ##

### Instruções para execução do App. ###

Acessando a pasta do projeto pelo terminal.

* Executar o comando "yarn";
* Executar "yarn start";
* Em outro terminal, na pasta do projeto, executar o "yarn android" ou "yarn ios";


### Descrição das libs utilizadas: ###

* @react-native-picker/picker -> Componente nativo de picker;
* axios -> Cliente HTTP baseado em Promises para fazer requisições;
* immer -> Utilizado para criar a árvore imutável no Redux;
* prop-types -> Verificação de tipo de tempo de execução para props React e objetos semelhantes;
* react-native-gesture-handler -> ;
* react-navigation -> Lib para navegação dentro do App;
* react-redux, redux, redux-saga -> Utilizado para controle de estado;
* styled-components -> Lib para utilizar código em CSS para estilizar os componentes;
